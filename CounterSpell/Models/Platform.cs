﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CounterSpell.Models
{
    public class Platform
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Image[] Images { get; set; }
    }
}

