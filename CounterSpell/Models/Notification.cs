﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterSpell.Models
{
    public abstract class Notification
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
