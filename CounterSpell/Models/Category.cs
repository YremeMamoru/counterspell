﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterSpellModels
{
    class Category
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }    }
}
