﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterSpell.Models
{
    public abstract class User
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
        public string Status { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Exp { get; set; }
        public bool IsDev { get; set; }
        public Image[] image { get; set; }
        //TODO: All Requests 
        public void Request(int idUser, string Name, string description)
        {

        }

        public void SendReview(Game game, string comment, short ovrRating, Dictionary<string, short> ratings)
        {

        }

        public void Report(User user, string description)
        {
            Report report = new Report(user, description);


        }

        public void Report(Review review, string description)
        {
            Report report = new Report(review, description);


        }
    }
}
