﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterSpell.Models
{
    public class News
    {
        public int Id { get; set; }
        public string Headline { get; set; }
        public string Body { get; set; }
        public Image[] Images { get; set; }
    }
}
