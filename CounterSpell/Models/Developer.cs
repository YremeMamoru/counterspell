﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterSpellModels
{
    public class Developer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Image[] Images { get; set; }
    }
}
