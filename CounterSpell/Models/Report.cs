﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterSpell.Models
{
    public class Report : Notification
    {
        public bool IsUser { get; set; }
        public bool IsReview { get; set; }

        public Report(User user, string description)
        {
            this.IsUser = true;
            Description += String.Format("Usuário: {0} \n Descrição: {1]", user.Nickname, description);
        }

        public Report(Review review, string description)
        {
            this.IsReview = true;
            Description += String.Format("Jogo: {0} \n Review: {1} \n\n Descrição: {2]", review.game.Name, review.Comment, description);
        }

    }
}
