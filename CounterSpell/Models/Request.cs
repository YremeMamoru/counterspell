﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterSpell.Models
{
    public class Request : Notification
    {
        public bool IsGame { get; set; }
        public bool IsPerson { get; set; }
    }
}
