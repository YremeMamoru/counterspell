﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterSpell.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public short OverallRating { get; set; }
        public Game game { get; set; }
        public Dictionary<string, short> Ratings { get; set; }
    }
}
